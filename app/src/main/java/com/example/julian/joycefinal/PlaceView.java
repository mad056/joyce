package com.example.julian.joycefinal;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.example.julian.joycefinal.databinding.ActivityPlaceViewBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class PlaceView extends AppCompatActivity  {

    public static String CODEREFERENCEVIEW ;
    public static String IMAGEREF ;

        public ActivityPlaceViewBinding mBinding;
        ViewPager viewPager;
        LinearLayout sliderDots;
        public int dotCounts;
        public ImageView[] dots;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_place_view);
            mBinding = DataBindingUtil.setContentView(this,R.layout.activity_place_view);
            LoadData();
            viewPager=mBinding.viewPager;
            sliderDots=mBinding.SliderDots;
            ViewPageAdapter viewPageAdapter=new ViewPageAdapter(this);
            viewPager.setAdapter(viewPageAdapter);
            dotCounts=viewPageAdapter.getCount();
            dots = new ImageView[dotCounts];

            if (ViewPageAdapter.images.size() > 0) {
                for(int i=0;i<dotCounts;i++){
                    dots[i]=new ImageView(this);
                    dots[i].setImageDrawable(ContextCompat.getDrawable(PlaceView.this,R.drawable.nonactive_dot));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(8, 0, 8, 0);
                    sliderDots.addView(dots[i], params);
                }

                dots[0].setImageDrawable(ContextCompat.getDrawable(PlaceView.this, R.drawable.active_dot));
                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {

                        for(int i = 0; i< dotCounts; i++){
                            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                        }
                        dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
                Timer timer = new Timer();
                timer.scheduleAtFixedRate(new myTimerTask(), 4000 ,4000);
            }

        }



    public void LoadData() {
        final EditText Name = (EditText) findViewById(R.id.editname10);
        final EditText Comment = (EditText) findViewById(R.id.EditJC9);
        final EditText JoyceComment = (EditText) findViewById(R.id.EditJJC9);
        final EditText Lat = (EditText) findViewById(R.id.editlat3);
        final EditText Long = (EditText) findViewById(R.id.editlong);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference();
        mdatabaseReference.child("Places").child(CODEREFERENCEVIEW).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals("Name")) {
                        Name.setText(answerSnapshot.getValue(String.class));
                        //System.out.println(Name);
                    }

                    if (answerSnapshot.getKey().equals("JoyceComment")) {
                        JoyceComment.setText(answerSnapshot.getValue(String.class));
                        //System.out.println(Name);
                    }

                    if (answerSnapshot.getKey().equals("Lat")) {
                        Lat.setText(String.valueOf(answerSnapshot.getValue(Double.class)));

                    }

                    if (answerSnapshot.getKey().equals("Long")) {
                        Long.setText(String.valueOf(answerSnapshot.getValue(Double.class)));

                    }

                    if (answerSnapshot.getKey().equals("Comment")) {
                        Comment.setText(answerSnapshot.getValue(String.class));
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }





    public class myTimerTask extends TimerTask {
            @Override
            public void run() {

                PlaceView.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(viewPager.getCurrentItem() == 0){
                            viewPager.setCurrentItem(1);
                        } else if(viewPager.getCurrentItem() == 1){
                            viewPager.setCurrentItem(2);
                        } else
                        {
                            viewPager.setCurrentItem(0);
                        }

                    }
                });
            }
        }

}
