package com.example.julian.joycefinal;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.commons.lang3.ObjectUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MainScreen extends AppCompatActivity {
    public static Long Days;
    TextView Luv;
    public static ArrayList<Status> Statuses = new java.util.ArrayList<>();


    String trigger;
    ImageButton JoyceButton;
    TextView JoyceText;

    public static Double CurrnetLat;
    public static Double CurrentLong;
    Handler handler = new Handler();
    Double lat = Double.valueOf(0);
    Double lng = Double.valueOf(0);
    public static Timestamp Juliantime;
    public static Double Julianlat;
    public static Double Julianlng;
    public static Timestamp Joycetime;
    public static Double Joycelat;
    public static Double Joycelng;
    public static Timestamp time;
    public static Double temppulse;

// To do
    // Joyce Version
    // Fix Splash
    // Interernet checks
    // Two version ehcek
    // marker replace
    // Maybe add slider to main screen
    // fix the marker default point.

    public boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        JoyceButton = (ImageButton) findViewById(R.id.JoyceToolButton);
        JoyceText = (TextView) findViewById(R.id.textView3);
        AnnCheck();

        // Grab Status
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        try {
            FirebaseDatabase data = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = data.getReference();

            Statuses.clear();
            MaxDatabase.child("Status").child("Status").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        Status status = new Status();

                        status.SetStatus(answerSnapshot.getKey());
                        status.SetStatusDes(answerSnapshot.getValue(String.class));
                        System.out.println(answerSnapshot.getKey() + " ADDED");
                        Statuses.add(status);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            //  Calculate Time Between 26/1/2018 and today!
            Date startdate = new Date("1/26/2018"); //deprecated
            Date now = new Date();
            long diff = now.getTime() - startdate.getTime();
            Days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            Luv = (TextView) findViewById(R.id.love);
            Luv.setText(String.valueOf(Days) + " " + "Days");

            if (isConnected()) {
                //Connected to the internet
                UpdateLocation();
                GrabLocation();
                CheckPulse();
            } else {
                //Handle error and show toast
                Toast.makeText(MainScreen.this, "No Internet Detected!", Toast.LENGTH_LONG).show();

            }


            RunHandler();

        } catch (SecurityException e) {
            new AlertDialog.Builder(this)
                    .setTitle("Permission Error")
                    .setMessage("Please Enable GPS Permissions For The Application To Work")
                    .setPositiveButton(android.R.string.ok, null)
                    .show();

        }
    }


    private void RunHandler() {
        handler = new Handler(); // new handler
        handler.postDelayed(runnable, 5000); // 10 mins int.
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            /* my set of codes for repeated work */
            if (isConnected()) {
                //Connected to the internet
                GrabLocation();
                CompareLoc();
                CheckPulse();
            } else {
                //Handle error and show toast
                Toast.makeText(MainScreen.this, "No Internet Detected!", Toast.LENGTH_LONG).show();

            }


            handler.postDelayed(runnable, 300000); // reschedule the handler
        }
    };


    // Download and display them in the listview.


    // Maps to maps

    public void MapsBtnClicked(View view) {
        startActivity(new Intent(MainScreen.this, PastNavigate.class));

    }
    // About to About Click

    public void AboutBtnClicked(View view) {
        startActivity(new Intent(MainScreen.this, About.class));
    }

    public void FutureNavigateBtnClicked(View view) {
        startActivity(new Intent(MainScreen.this, FutureNavigate.class));


    }

    public void ToolBtnClicked(View view) {
      //  startActivity(new Intent(MainScreen.this, Tools.class));
        Toast.makeText(MainScreen.this, "Not enabled yet. Stay Tuned la!", Toast.LENGTH_LONG).show();


    }

    public void JoyceToolBtnClicked(View view) {
        startActivity(new Intent(MainScreen.this, JoyceTool.class));


    }

    // We need a public method to grab their location

    // Grab Location from Google.
    public void UpdateLocation() {

        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();


        MaxDatabase.child("Pulse").child("Joyce").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {


                    if (answerSnapshot.getKey().equals("Time")) {

                        System.out.println("FUKC " + answerSnapshot.getValue(String.class));
                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date parsedDate = dateFormat.parse(answerSnapshot.getValue(String.class));
                            time = new java.sql.Timestamp(parsedDate.getTime());
                        } catch (Exception e) { //this generic but you can control another types of exception
                            // look the origin of excption
                        }
                    }

                    if (answerSnapshot.getKey().equals("LocationLat")) {

                        lat = answerSnapshot.getValue(Double.class);

                    }

                    if (answerSnapshot.getKey().equals("LocationLong")) {
                        lng = answerSnapshot.getValue(Double.class);


                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Compare

    }


    public void CompareLoc() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());


            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference mdatabaseReference = database.getReference("Pulse").child("Joyce");

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String string = dateFormat.format(timestamp);
            System.out.println(string);

            if (CurrnetLat != null) {
                mdatabaseReference.child("Time").setValue(string);
                mdatabaseReference .child("LocationLat").setValue(CurrnetLat);
                mdatabaseReference.child("LocationLong").setValue(CurrentLong);

                Julianlat = CurrnetLat;
                Julianlng = CurrentLong;
                Juliantime = timestamp;
            }


        }




    public void GrabLocation() {
        try {
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            CurrnetLat = location.getLatitude();
            CurrentLong = location.getLongitude();


        } catch (NullPointerException name) {
            Julianlat = lat;
            Julianlng = lng;
        }
    }



    public void CheckPulse() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Pulse").child("Joyce").addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals("LatestPulse")) {
                        temppulse = answerSnapshot.getValue(Double.class);

                        break;
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                try {
                    if (temppulse != 1337.0) {
                        // Set data to null
                        // Send pyshg

                        SendNotification(String.valueOf(temppulse));
                        System.out.println("RESET");


                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference mdatabaseReference = database.getReference("Pulse").child("Joyce");
                        mdatabaseReference.child("LatestPulse").setValue(1337);
                        temppulse = 1337.0;

                    }
                } catch(NullPointerException lmao) {

            }

            }
        }, 5000); //Timer is in ms here.


    }

    public void SendNotification(String a) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "1")
                .setSmallIcon(R.mipmap.heart) //icon
                .setContentTitle("Pulse From Julian")
                .setContentText("He is " + a + " KM away!")
                .setAutoCancel(true)//swipe for delete
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, mBuilder.build());
    }

    private void AnnCheck() {

        SharedPreferences settings = this.getSharedPreferences("Onboarding", 0);
        boolean firstTime = settings.getBoolean("first_time", true);

        if (firstTime) {
            startActivity(new Intent(MainScreen.this, Onboarding.class));

        }

    }


}
