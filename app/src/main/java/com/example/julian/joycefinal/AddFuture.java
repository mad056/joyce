package com.example.julian.joycefinal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AddFuture extends AppCompatActivity {

    static final int PICK_MAP_POINT_REQUEST = 999;  // The request code
    private ProgressDialog mProgressDialog;
    private Integer Count;
    public static String LoadMethod;

    private int index ;
    private double lat;
    private double loong;
    private String comment;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_future);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        index = 1;

        SelectView(index);
        mProgressDialog = new ProgressDialog(this);

        if (LoadMethod.equals("Edit")) {
            LoadFromEdit();
        }

        if (LoadMethod.equals("Create")) {
            LoadCount();

        }
    }


    public void SubmitEntryBtn2(View view) {
        if (index == 2) {
            final EditText Commedit = (EditText) findViewById(R.id.commentEdit2);
            final EditText nameedit = (EditText) findViewById(R.id.nameEdit2);

            name = nameedit.getText().toString();
            comment = Commedit.getText().toString();

        }
        if (index == 3) {
            index = 1;
            if (LoadMethod.equals("Edit")) {

                submitEdit();
            }

            if (LoadMethod.equals("Create")) {
                submitCreate();

            }

        } else {
            index += 1;
            SelectView(index);
        }



    }


    private void submitCreate() {
        final EditText Name = (EditText) findViewById(R.id.nameEdit2);
        final EditText Comment = (EditText) findViewById(R.id.commentEdit2);
        final EditText Lat = (EditText) findViewById(R.id.latEdit2);
        final EditText Long = (EditText) findViewById(R.id.longEdit3);

        Upcount(String.valueOf(Count));
        String Templat = Lat.getText().toString();
        String Templong = Long.getText().toString();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("FuturePlaces/" + String.valueOf(Count));
        mdatabaseReference.child("Comment").setValue(Comment.getText().toString());
        mdatabaseReference.child("Name").setValue(Name.getText().toString());
        mdatabaseReference.child("Lat").setValue(Double.valueOf(Templat));
        mdatabaseReference.child("Long").setValue(Double.valueOf(Templong));

        Toast.makeText(AddFuture.this, "Written to Database. Success la!", Toast.LENGTH_LONG).show();
        startActivity(new Intent(AddFuture.this, FutureNavigate.class));

    }

    private void LoadFromEdit() {
        final EditText Name = (EditText) findViewById(R.id.nameEdit2);
        final EditText Comment = (EditText) findViewById(R.id.commentEdit2);
        final EditText Lat = (EditText) findViewById(R.id.latEdit2);
        final EditText Long = (EditText) findViewById(R.id.longEdit3);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference();
        mdatabaseReference.child("FuturePlaces").child(PlaceView.CODEREFERENCEVIEW).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Count = Integer.valueOf(dataSnapshot.getKey());
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals("Name")) {
                        Name.setText(answerSnapshot.getValue(String.class));
                        //System.out.println(Name);
                    }


                    if (answerSnapshot.getKey().equals("Lat")) {
                        Lat.setText(String.valueOf(answerSnapshot.getValue(Double.class)));

                    }

                    if (answerSnapshot.getKey().equals("Long")) {
                        Long.setText(String.valueOf(answerSnapshot.getValue(Double.class)));

                    }

                    if (answerSnapshot.getKey().equals("Comment")) {
                        Comment.setText(answerSnapshot.getValue(String.class));
                    }


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void LoadCount() {

        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("FuturePlaces").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals("Count")) {
                        Count = answerSnapshot.getValue(Integer.class);
                        Count += 1;
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void submitEdit() {
        final EditText Name = (EditText) findViewById(R.id.nameEdit2);
        final EditText Comment = (EditText) findViewById(R.id.commentEdit2);
        final EditText Lat = (EditText) findViewById(R.id.latEdit2);
        final EditText Long = (EditText) findViewById(R.id.longEdit3);
        String Templat = Lat.getText().toString();
        String Templong = Long.getText().toString();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("FuturePlaces/" + String.valueOf(PlaceView.CODEREFERENCEVIEW));
        mdatabaseReference.child("Comment").setValue(Comment.getText().toString());
        mdatabaseReference.child("Name").setValue(Name.getText().toString());
        mdatabaseReference.child("Lat").setValue(Double.valueOf(Templat));
        mdatabaseReference.child("Long").setValue(Double.valueOf(Templong));
        Toast.makeText(AddFuture.this, "Edited to Database. Success la!", Toast.LENGTH_LONG).show();
        startActivity(new Intent(AddFuture.this, FutureNavigate.class));
    }

    public void AddLoc2(View view) {
        pickPointOnMap();

    }

    private void pickPointOnMap() {
        Intent pickPointIntent = new Intent(this, PickMap.class);


        startActivityForResult(pickPointIntent, PICK_MAP_POINT_REQUEST);
    }




    protected void Upcount(String A) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("FuturePlaces");
        mdatabaseReference.child("Count").setValue(Integer.valueOf(A));


    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {

            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {

                    case 999:
                        if (resultCode == RESULT_OK) {
                            LatLng latLng = (LatLng) data.getParcelableExtra("picked_point");
                            String templocname = (String) data.getStringExtra("title");

                            final EditText lat = (EditText) findViewById(R.id.latEdit2);
                            final EditText long2 = (EditText) findViewById(R.id.longEdit3);
                            lat.setText(String.valueOf(latLng.latitude));
                            long2.setText(String.valueOf(latLng.longitude));
                            name = templocname;


                    }

                            // Toast.makeText(this, "Point Chosen: " + latLng.latitude + " " + latLng.longitude, Toast.LENGTH_LONG).show();
                        }
                }



    }

    public void SelectView(int i) {
        final TextView Name = (TextView) findViewById(R.id.nameText2);
        final TextView ClicktoBegin = (TextView) findViewById(R.id.textView20);
        final ImageButton SetLoc = (ImageButton) findViewById(R.id.locationSelBtn2);
        final EditText Latedit = (EditText) findViewById(R.id.latEdit2);
        final EditText LongEdit = (EditText) findViewById(R.id.longEdit3);
        final ImageButton Submit = (ImageButton) findViewById(R.id.submitEntryBtn2);
        final EditText Commedit = (EditText) findViewById(R.id.commentEdit2);
        final EditText nameedit = (EditText) findViewById(R.id.nameEdit2);

        final TextView Heading = (TextView) findViewById(R.id.textView21);
        final TextView commentname = (TextView) findViewById(R.id.commentText2);
        RelativeLayout rl = (RelativeLayout)findViewById(R.id.relativeLayout2);

        switch (i) {

            case 1 :
                Name.setVisibility(View.INVISIBLE);
                Commedit.setVisibility(View.INVISIBLE);
                nameedit.setEnabled(false);
                nameedit.setVisibility(View.INVISIBLE);
                commentname.setVisibility(View.INVISIBLE);

                Heading.setText("Select Location");
                Submit.setImageResource(R.drawable.continuepur);
                ClicktoBegin.setVisibility(View.VISIBLE);
                SetLoc.setVisibility(View.VISIBLE);
                SetLoc.setEnabled(true);
                Latedit.setVisibility(View.VISIBLE);
                LongEdit.setVisibility(View.VISIBLE);



                break;
            case 2 :

                SetLoc.setVisibility(View.INVISIBLE);
                SetLoc.setEnabled(false);
                Submit.setBackgroundColor(0x41A9CC);
                ClicktoBegin.setVisibility(View.INVISIBLE);
                rl.setBackgroundColor(getResources().getColor(R.color.backgroundvlue));
                commentname.setVisibility(View.VISIBLE);
                Commedit.setVisibility(View.VISIBLE);
                Name.setVisibility(View.VISIBLE);
                Latedit.setVisibility(View.VISIBLE);
                LongEdit.setVisibility(View.VISIBLE);
                Heading.setText("Add Comment");
                Submit.setImageResource(R.drawable.continueblue);
                nameedit.setEnabled(true);
                nameedit.setVisibility(View.VISIBLE);
                nameedit.setText(name);

                break;
            case 3 :
                Heading.setText("Summary");

                SetLoc.setVisibility(View.INVISIBLE);
                SetLoc.setEnabled(false);
                Submit.setBackgroundColor(0xFDBE57);
                ClicktoBegin.setVisibility(View.INVISIBLE);
                nameedit.setText(name);
                Commedit.setText(comment);
                rl.setBackgroundColor(getResources().getColor(R.color.backorange));
                commentname.setVisibility(View.VISIBLE);
                Commedit.setVisibility(View.VISIBLE);
                Name.setVisibility(View.VISIBLE);
                Latedit.setVisibility(View.VISIBLE);
                LongEdit.setVisibility(View.VISIBLE);
                Submit.setImageResource(R.drawable.gotitornage);
                nameedit.setEnabled(false);
                Commedit.setEnabled(false);
                nameedit.setVisibility(View.VISIBLE);
                break;

        }

    }

    public void CancelClick2(View view) {
        Toast.makeText(AddFuture.this, "Canceled.", Toast.LENGTH_LONG).show();

        startActivity(new Intent(AddFuture.this, FutureNavigate.class));

    }
}


