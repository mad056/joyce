package com.example.julian.joycefinal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;

public class FutureMap extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String tempName;
    Double templat = Double.valueOf(0);
    Double templong = Double.valueOf(0);
    private ProgressDialog mProgressDialog ;
    public String random;
    public Integer Max ;

    ArrayList<Marker> MarkerList = new ArrayList<Marker>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_future_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mProgressDialog = new ProgressDialog(this);
        MarkerList.clear();
        LoadData();
    }

    private void LoadData(){

        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        mProgressDialog.setMessage("Loading Data...");
        mProgressDialog.show();

        MaxDatabase.child("FuturePlaces").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                        if (InnerSnapshop.getKey().equals("Lat")) {
                            templat = InnerSnapshop.getValue(Double.class);

                        }
                        if (InnerSnapshop.getKey().equals("Long")) {
                            templong = InnerSnapshop.getValue(Double.class);
                        }
                        if (InnerSnapshop.getKey().equals("Name")) {
                            tempName = InnerSnapshop.getValue(String.class);
                        }

                    }
                    // Add Marker

                    AddMarker(templat, templong, tempName);
                    System.out.println("YES") ;
                }
                mProgressDialog.dismiss();

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void AddMarker(Double Lat, Double Long, String Name) {
        LatLng location = new LatLng(Lat, Long);
        MarkerList.add(mMap.addMarker(new MarkerOptions().position(location).title(Name)));
        mMap.addMarker(new MarkerOptions().position(location).title(Name));

    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 10));

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {


            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                View v = getLayoutInflater().inflate(R.layout.windowlayout, null);
                //LatLng latLng = marker.getPosition();
                // Getting the position from the marker
                LatLng latLng2 = marker.getPosition();

                // Getting reference to the TextView to set latitude
                TextView tvLat = (TextView) v.findViewById(R.id.tv_lat);
                TextView tvtitle = (TextView) v.findViewById(R.id.tv_title);

                // Getting reference to the TextView to set longitude
                TextView tvLng = (TextView) v.findViewById(R.id.tv_lng);

                // Setting the latitude
                tvLat.setText("Latitude:" + latLng2.latitude);
                tvtitle.setText(marker.getTitle());

                // Setting the longitude
                tvLng.setText("Longitude:" + latLng2.longitude);

                return v;
            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {

            @Override
            public boolean onMarkerClick(Marker arg0) {
                //  if(arg0 != null && arg0.getTitle().equals(markerOptions2.getTitle().toString())); // if marker  source is clicked
                arg0.showInfoWindow();
                return true;
            }

        });


        mMap.setOnInfoWindowLongClickListener(new GoogleMap.OnInfoWindowLongClickListener() {
            @Override
            public void onInfoWindowLongClick(final Marker marker) {
                String[] items={"Modify", "Been To!","Delete", };
                AlertDialog.Builder itemDilog = new AlertDialog.Builder(FutureMap.this);
                itemDilog.setTitle("Options");
                itemDilog.setCancelable(true);

                itemDilog.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Get the Key of the inspecting element in the DB
                        FirebaseDatabase data = FirebaseDatabase.getInstance();
                        DatabaseReference MaxDatabase = data.getReference();
                        MaxDatabase.child("FuturePlaces").orderByChild("Name").equalTo(marker.getTitle()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                                    String tempkey = answerSnapshot.getKey();
                                    System.out.println("DEBUG " + tempkey);
                                    PlaceView.CODEREFERENCEVIEW = tempkey;
                                }

                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                        switch(which){
                            case 0:{
                                AddFuture.LoadMethod = "Edit";
                                startActivity(new Intent(FutureMap.this, AddFuture.class));
                            }break;
                            case 1:{
                           BeenTo();
                            }break;

                            case 2:{
                                String[] items={"Yes", "No" };
                                AlertDialog.Builder itemDilog2 = new AlertDialog.Builder(FutureMap.this);
                                itemDilog2.setTitle("Are You Sure?");
                                itemDilog2.setCancelable(true);

                                itemDilog2.setItems(items, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch(which) {
                                            case 0: {
                                                Toast.makeText(FutureMap.this, "Deleted", Toast.LENGTH_LONG).show();
                                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                                DatabaseReference mdatabaseReference = database.getReference();
                                                mdatabaseReference.child("FuturePlaces").child(PlaceView.CODEREFERENCEVIEW).removeValue();
                                                marker.remove();
                                                DownCount();
                                                startActivity(new Intent(FutureMap.this, FutureMap.class));

                                            }
                                            break;
                                            case 1: {

                                            }
                                            break;
                                        }
                                    }
                                });
                                itemDilog2.show();                            }break;
                        }

                    }
                });
                itemDilog.show();

            }
        });
    }

    public void BeenTo() {
        mProgressDialog.setMessage("Loading Data...");
        mProgressDialog.show();
        AddPast.LoadMethod = "Future Convert";
        startActivity(new Intent(FutureMap.this, AddPast.class));


        mProgressDialog.dismiss();


    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(FutureMap.this, FutureNavigate.class));

    }
private void DownCount() {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mdatabaseReference = database.getReference("FuturePlaces");
    mdatabaseReference.child("Count").setValue((MarkerList.size() - 2));

}


    // MAPS SHIT

    public void RandomButtonClicked(View view) {
        Max = MarkerList.size();
        System.out.println(String.valueOf(MarkerList.size()));
        LoadRandom();


        Marker tempmarker = MarkerList.get(Integer.valueOf(random));
        LatLng latLng = tempmarker.getPosition();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 25));


    }



    protected void LoadRandom() {
        Random rand = new Random();

        int  n = rand.nextInt(Max)  ;
        random = Integer.toString(n);

    }


}
