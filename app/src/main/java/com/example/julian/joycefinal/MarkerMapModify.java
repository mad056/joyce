package com.example.julian.joycefinal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;

public class MarkerMapModify extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMa2p;

    String tempName;
    Double templat = Double.valueOf(0);
    Double templong = Double.valueOf(0);
   // final  LatLng sydney = new LatLng(-34, 151);
   private ProgressDialog mProgressDialog ;


    private String random;
    private Integer Max ;

    ArrayList<Marker> MarkerList = new ArrayList<Marker>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marker_map_modify);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment2 = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment2.getMapAsync(this);
        mProgressDialog = new ProgressDialog(this);

        LoadData();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */


    private void LoadData(){
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        mProgressDialog.setMessage("Loading Data...");
        mProgressDialog.show();

        MaxDatabase.child("Places").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                        if (InnerSnapshop.getKey().equals("Lat")) {
                           templat = InnerSnapshop.getValue(Double.class);

                        }
                        if (InnerSnapshop.getKey().equals("Long")) {
                            templong = InnerSnapshop.getValue(Double.class);
                        }
                        if (InnerSnapshop.getKey().equals("Name")) {
                            tempName = InnerSnapshop.getValue(String.class);
                        }

                    }
                    // Add Marker
                    AddMarker(templat, templong, tempName);
                    System.out.println("YES") ;
                }
                mProgressDialog.dismiss();

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(MarkerMapModify.this, PastNavigate.class));

    }

    private void AddMarker(Double Lat, Double Long, String Name) {
        LatLng location = new LatLng(Lat, Long);
        MarkerList.add(mMa2p.addMarker(new MarkerOptions().position(location).title(Name)));

        mMa2p.addMarker(new MarkerOptions().position(location).title(Name));

    }

     @Override
    public void onMapReady(GoogleMap googleMap) {

         mMa2p = googleMap;

        // Add a marker in Sydney and move the camera
       LatLng sydney = new LatLng(-34, 151);
         mMa2p.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
         mMa2p.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));

         mMa2p.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {


                                        @Override
                                        public View getInfoWindow(Marker marker) {
                                            return null;
                                        }

                                        @Override
                                        public View getInfoContents(Marker marker) {

                                            View v = getLayoutInflater().inflate(R.layout.windowlayout, null);
                                            LatLng latLng = marker.getPosition();
                                            // Getting the position from the marker
                                            LatLng latLng2 = marker.getPosition();

                                            // Getting reference to the TextView to set latitude
                                            TextView tvLat = (TextView) v.findViewById(R.id.tv_lat);
                                            TextView tvtitle = (TextView) v.findViewById(R.id.tv_title);

                                            // Getting reference to the TextView to set longitude
                                            TextView tvLng = (TextView) v.findViewById(R.id.tv_lng);

                                            // Setting the latitude
                                            tvLat.setText("Latitude:" + latLng2.latitude);
                                            tvtitle.setText(marker.getTitle());

                                            // Setting the longitude
                                            tvLng.setText("Longitude:" + latLng2.longitude);

                                            return v;
                                        }
                                    });
         mMa2p.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
         {

             @Override
             public boolean onMarkerClick(Marker arg0) {
               //  if(arg0 != null && arg0.getTitle().equals(markerOptions2.getTitle().toString())); // if marker  source is clicked
                 arg0.showInfoWindow();
                 return true;
             }

         });


         mMa2p.setOnInfoWindowLongClickListener(new GoogleMap.OnInfoWindowLongClickListener() {
             @Override
             public void onInfoWindowLongClick(final Marker marker) {
                 String[] items={"Modify", "View","Delete", };
                 AlertDialog.Builder itemDilog = new AlertDialog.Builder(MarkerMapModify.this);
                 itemDilog.setTitle("Options");
                 itemDilog.setCancelable(true);

                 itemDilog.setItems(items, new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int which) {
                         // Get the Key of the inspecting element in the DB
                         FirebaseDatabase data = FirebaseDatabase.getInstance();
                         DatabaseReference MaxDatabase = data.getReference();
                         MaxDatabase.child("Places").orderByChild("Name").equalTo(marker.getTitle()).addValueEventListener(new ValueEventListener() {
                             @Override
                             public void onDataChange(DataSnapshot dataSnapshot) {
                                 for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                                    String tempkey = answerSnapshot.getKey();
                                    System.out.println("DEBUG " + tempkey);
                                     PlaceView.CODEREFERENCEVIEW = tempkey;
                                 }

                             }
                             @Override
                             public void onCancelled(DatabaseError databaseError) {

                             }
                         });


                         switch(which){
                             case 0:{
                                 AddPast.LoadMethod = "Edit";
                                 startActivity(new Intent(MarkerMapModify.this, AddPast.class));
                             }break;
                             case 1:{
                                 LoadImages();
                                 mProgressDialog.setMessage("Loading Data...");
                                 mProgressDialog.show();

                                 new Handler().postDelayed(new Runnable() {
                                     @Override
                                     public void run() {
                                         mProgressDialog.dismiss();
                                         startActivity(new Intent(MarkerMapModify.this, PlaceView.class));

                                     }
                                 }, 2000); //Timer is in ms here.

                             }break;

                             case 2:{
                                 String[] items={"Yes", "No" };
                                 AlertDialog.Builder itemDilog2 = new AlertDialog.Builder(MarkerMapModify.this);
                                 itemDilog2.setTitle("Are You Sure?");
                                 itemDilog2.setCancelable(true);

                                 itemDilog2.setItems(items, new DialogInterface.OnClickListener() {
                                             public void onClick(DialogInterface dialog, int which) {
                                                 switch(which) {
                                                     case 0: {
                                                         Toast.makeText(MarkerMapModify.this, "Deleted", Toast.LENGTH_LONG).show();
                                                         FirebaseDatabase database = FirebaseDatabase.getInstance();
                                                         DatabaseReference mdatabaseReference = database.getReference();
                                                         mdatabaseReference.child("Places").child(PlaceView.CODEREFERENCEVIEW).removeValue();
                                                         marker.remove();
                                                         startActivity(new Intent(MarkerMapModify.this, MarkerMapModify.class));

                                                     }
                                                     break;
                                                     case 1: {

                                                     }
                                                     break;
                                                 }
                                             }
                                         });
                                         itemDilog2.show();
                             }break;
                         }

                     }
                 });
                 itemDilog.show();

             }
         });
    }


    public void LoadImages() {

        // List The Entries from the PlacesImages Storage.
        ViewPageAdapter.images.clear();
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("PlaceImages").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    if (answerSnapshot.getKey().equals(PlaceView.CODEREFERENCEVIEW)) {
                        for (DataSnapshot InnerSnap : answerSnapshot.getChildren()) {
                            String tempkey = InnerSnap.getValue(String.class);
                            ViewPageAdapter.images.add(tempkey);
                            System.out.println(tempkey);
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Download and display them in the listview.




    }

    // MAPS SHIT

    public void RandomPastButtonClicked(View view) {
        Max = MarkerList.size();
        LoadRandom();


        Marker tempmarker = MarkerList.get(Integer.valueOf(random));
    //    tempmarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.heart));
        LatLng latLng = tempmarker.getPosition();
       // System.out.println("TITLE ITS WORKING: " + String.valueOf(latLng.latitude));
        mMa2p.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 25));


    }



    protected void LoadRandom() {
        Random rand = new Random();

        int  n = rand.nextInt(Max) ;
        random = Integer.toString(n);

    }


}

