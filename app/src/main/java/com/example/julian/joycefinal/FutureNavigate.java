package com.example.julian.joycefinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class FutureNavigate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_future_navigate);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }


    public void AddFButton(View view) {
        AddFuture.LoadMethod = "Create";
        startActivity(new Intent(FutureNavigate.this, AddFuture.class));

    }
    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(FutureNavigate.this, MainScreen.class));

    }

    public void ViewFButton(View view) {
        startActivity(new Intent(FutureNavigate.this, FutureMap.class));

    }

    public void ListFButton(View view) {
        startActivity(new Intent(FutureNavigate.this, ModifyFuture.class));

    }




    }

