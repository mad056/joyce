package com.example.julian.joycefinal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import android.graphics.Color;
import android.widget.Toast;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class About extends AppCompatActivity {
    public final double  CurrentVersion = 1.6;
    public  String  trigger;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    public String UpdateURl;
    private ListView list;
    private ProgressDialog mProgressDialog ;

    public TextView Update;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Checking For Updates");
        mProgressDialog.show();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Update = (TextView) findViewById(R.id.AboutUpdate);

        Update.setText("Version: " + String.valueOf(CurrentVersion));
     // Grab Version Number

        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("Status").child("Version Data").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals("tigger")) {
                        Update.setText("Version: " + String.valueOf(CurrentVersion));
                        System.out.println(answerSnapshot.getValue(String.class));
                        trigger = answerSnapshot.getValue(String.class);
                    }

                    if (answerSnapshot.getKey().equals("updateurl")) {
                        UpdateURl = answerSnapshot.getValue(String.class);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // get update URL

        // and update status

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                try {

                    TextView UpdateText = (TextView) findViewById(R.id.UpdateText);

                    if (trigger.contentEquals("yes")) {
                        UpdateText.setText("Update Found");
                        UpdateText.setTextColor(Color.RED);


                    } else {
                        UpdateText.setText("No Update Found");
                        UpdateText.setTextColor(Color.GREEN);
                    }
                    mProgressDialog.dismiss();

                } catch(NullPointerException lmao) {

                }

            }
        }, 5000); //Timer is in ms here.




        // Grab Status Updates


        // Load Status Into ListView
        list = (ListView) findViewById(R.id.StatusList);


        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, R.layout.mylist, arrayList);
        list.setAdapter(adapter);



        registerForContextMenu(list);
        for (Status obj : MainScreen.Statuses) {

            arrayList.add(obj.GetStatus());
            adapter.notifyDataSetChanged();
        }

    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.StatusList) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle(arrayList.get(info.position));
            String[] menuItems = getResources().getStringArray(R.array.menu2);
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu2);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = arrayList.get(info.position);

        //System.out.println("Debug 1 :" + listItemName);
        for (Status obj :  MainScreen.Statuses) {
            if (listItemName.equals(obj.GetStatus())) {
                Toast.makeText(getApplicationContext(), obj.GetStatusDes(), Toast.LENGTH_LONG).show();
            }
        }




        return true;
    }


    public void UpdateBtnClicked(View view) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(UpdateURl));
        startActivity(browserIntent);

    }
    public void RequestBtnClicked(View view) {
        //  PlaceEdit.CODEREFERENCEVIEW = "1";
        startActivity(new Intent(About.this, FeatureRequest.class));
        //startActivity(new Intent(MainScreen.this, MarkerMapModify.class));

    }
}
