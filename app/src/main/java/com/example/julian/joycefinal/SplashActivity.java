package com.example.julian.joycefinal;

import android.content.Intent;

import android.os.Bundle;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Start home activity

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainScreen.class));
                finish();
            }
        }, 2000); //Timer is in ms here.


        // close splash activity



    }

}