package com.example.julian.joycefinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class PastNavigate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_past_navigate);
    }

    public void ModifyPastBtnClicked(View view) {

        startActivity(new Intent(PastNavigate.this, ModifyPast.class));

    }

    public void AddPastBtClicked(View view) {
        AddPast.LoadMethod = "Create";

        startActivity(new Intent(PastNavigate.this, AddPast.class));

    }
    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(PastNavigate.this, MainScreen.class));

    }
    public void ViewPastBtnClicked(View view) {

        startActivity(new Intent(PastNavigate.this, MarkerMapModify.class));

    }
}
