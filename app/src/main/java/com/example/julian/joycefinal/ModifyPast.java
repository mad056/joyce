package com.example.julian.joycefinal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ModifyPast extends AppCompatActivity {
    public static ArrayList<Status> PastPlaces = new java.util.ArrayList<>();
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    private ListView list;

    private ProgressDialog mProgressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_past);
        list = (ListView) findViewById(R.id.PastList);
        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, R.layout.mylist, arrayList);
        list.setAdapter(adapter);
        mProgressDialog = new ProgressDialog(this);
        LoadData();
        registerForContextMenu(list);

    }

    private void LoadData() {
        mProgressDialog.setMessage("Loading Data...");
        mProgressDialog.show();
            FirebaseDatabase data = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = data.getReference();


            MaxDatabase.child("Places").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Name")) {
                                arrayList.add(InnerSnapshop.getValue(String.class));
                                adapter.notifyDataSetChanged();
                            }

                        }

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            mProgressDialog.dismiss();

        }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.PastList) {

            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

// Get the Key of the inspecting element in the DB

            FirebaseDatabase data = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = data.getReference();
            MaxDatabase.child("Places").orderByChild("Name").equalTo(arrayList.get(info.position)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        String tempkey = answerSnapshot.getKey();
                        System.out.println("DEBUG " + tempkey);
                        PlaceView.CODEREFERENCEVIEW = tempkey;
                    }

                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            menu.setHeaderTitle(arrayList.get(info.position));
            String[] menuItems = getResources().getStringArray(R.array.menu3);
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu3);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = arrayList.get(info.position);

        if (menuItemName.equals("Modify")) {
            AddPast.LoadMethod = "Edit";
            startActivity(new Intent(ModifyPast.this, AddPast.class));
        }


        if (menuItemName.equals("View")) {

            LoadImages();
            mProgressDialog.setMessage("Loading Data...");
            mProgressDialog.show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mProgressDialog.dismiss();
                    startActivity(new Intent(ModifyPast.this, PlaceView.class));

                }
            }, 2000); //Timer is in ms here.

        }


        if (menuItemName.equals("Delete")) {
            String[] items={"Yes", "No" };
            AlertDialog.Builder itemDilog2 = new AlertDialog.Builder(ModifyPast.this);
            itemDilog2.setTitle("Are You Sure?");
            itemDilog2.setCancelable(true);

            itemDilog2.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch(which) {
                        case 0: {
                            Toast.makeText(ModifyPast.this, "Deleted", Toast.LENGTH_LONG).show();
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference mdatabaseReference = database.getReference();
                            mdatabaseReference.child("Places").child(PlaceView.CODEREFERENCEVIEW).removeValue();
                        }
                        break;
                        case 1: {

                        }
                        break;
                    }
                }
            });
            itemDilog2.show();
        }
        return true;
    }


    public void LoadImages() {

        // List The Entries from the PlacesImages Storage.
        ViewPageAdapter.images.clear();
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("PlaceImages").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    if (answerSnapshot.getKey().equals(PlaceView.CODEREFERENCEVIEW)) {
                        for (DataSnapshot InnerSnap : answerSnapshot.getChildren()) {
                            String tempkey = InnerSnap.getValue(String.class);
                            ViewPageAdapter.images.add(tempkey);
                            System.out.println(tempkey);
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Download and display them in the listview.




    }

}
