package com.example.julian.joycefinal;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Pulse extends AppCompatActivity {
    //public static Double temppulse = 1337.0;

    public static Double CurrnetLat;
    public static Double CurrentLong;

    public static Timestamp time;
    public static Double lat;
    public static Double lng;

    public static Timestamp Juliantime;
    public static Double Julianlat;
    public static Double Julianlng;

    public static Timestamp Joycetime;
    public static Double Joycelat;
    public static Double Joycelng;

   // Double[] JoyceLoc = new Double[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulse);


    }

    // We need a public method to grab their location

    // Grab Location from Google. *
    /*
    public void UpdateLocation() {

        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();


        MaxDatabase.child("Pulse").child("Julian").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {


                    if (answerSnapshot.getKey().equals("Time")) {

                        System.out.println("FUKC "+ answerSnapshot.getValue(String.class));
                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            Date parsedDate = dateFormat.parse(answerSnapshot.getValue(String.class));
                            time = new java.sql.Timestamp(parsedDate.getTime());
                        } catch(Exception e) { //this generic but you can control another types of exception
                            // look the origin of excption
                        }
                    }

                    if (answerSnapshot.getKey().equals("LocationLat")) {

                        lat = answerSnapshot.getValue(Double.class);

                    }

                    if (answerSnapshot.getKey().equals("LocationLong")) {
                        lng = answerSnapshot.getValue(Double.class);


                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Compare

    }



    public void CompareLoc() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());



        if (timestamp.compareTo(time) < 0) ;
        {
            Juliantime = time;
            Julianlat = lat;
            Julianlng = lng;

        }

        // Update Data.
        if (timestamp.compareTo(time) > 0) ;
        {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference mdatabaseReference = database.getReference("Pulse").child("Julian");

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String string  = dateFormat.format(timestamp);
            System.out.println(string);

            // FIX THIS FIX DATABASE
            mdatabaseReference.child("Time").setValue(string);
            mdatabaseReference.child("LocationLat").setValue(CurrnetLat);
            mdatabaseReference.child("LocationLong").setValue(CurrentLong);

            Julianlat = CurrnetLat;
            Julianlng = CurrentLong;
            Juliantime = timestamp;

        }

    }

    public  void GrabLocation() {

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        CurrnetLat = location.getLongitude();
        CurrentLong = location.getLatitude();

    }




    // Pulse
    private void Pulse() {

        // Grab and update Joyce's Locational Data
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();


        MaxDatabase.child("Pulse").child("Joyce").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    switch (answerSnapshot.getKey()) {
                        case "Time":
                            Joycetime = Timestamp.valueOf(answerSnapshot.getValue(String.class));
                        case "LocationLat":
                            Joycelat = answerSnapshot.getValue(Double.class);
                        case "LocationLong":
                            Joycelng = answerSnapshot.getValue(Double.class);
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


// Distance
        Location selected_location = new Location("locationA");
        selected_location.setLatitude(MainScreen.Julianlat);
        selected_location.setLongitude(MainScreen.Julianlng);
        Location near_locations = new Location("locationB");
        near_locations.setLatitude(Joycelat);
        near_locations.setLongitude(Joycelng);
        double distance = selected_location.distanceTo(near_locations);

// Write it to Databnase

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Pulse").child("Julian");
        mdatabaseReference.child("LatestPulse").setValue(distance);


    }



    public static void CheckPulse () {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Pulse").child("Julian").addValueEventListener(new ValueEventListener() {

            public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                if (answerSnapshot.getKey().equals("LatestPulse")) {
                    temppulse = answerSnapshot.getValue(Double.class);
                    System.out.println("FUCK2 " +  String.valueOf(answerSnapshot.getValue(Double.class)));
                    break;
                }

            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });

        // Send pyshg
        if (temppulse != 1337) {
            Pulse pulse = new Pulse();
            pulse.SendNotification(String.valueOf(temppulse));

        }

// Set data to null
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Pulse").child("Julian");
        mdatabaseReference.child("LatestPulse").setValue(1337);
        temppulse = 1337.0;


    }

    private void SendNotification(String a) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(Pulse.this, "1")
                .setContentTitle("Pulse From Joyce")
                .setContentText("She is "  + a + " meters away!" )
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        Log.d("FUCK", a);

    }*/
}