package com.example.julian.joycefinal;

import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class JoyceTool extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_joyce_tool);
    }


    public void PulseBtnClicked(View view) {
        Pulse();

    }
    public void whyBtnClicked(View view) {
        startActivity(new Intent(JoyceTool.this, Reasons.class));

    }

    private void Pulse() {
        System.out.println("DISTANCE" );

        // Grab and update Joyce's Locational Data
        FirebaseDatabase data2 = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase2 = data2.getReference();


        MaxDatabase2.child("Pulse").child("Julian").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {


                    if (answerSnapshot.getKey().equals("Time")) {

                        System.out.println("FUKC "+ answerSnapshot.getValue(String.class));
                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            Date parsedDate = dateFormat.parse(answerSnapshot.getValue(String.class));
                            MainScreen.Joycetime = new java.sql.Timestamp(parsedDate.getTime());
                        } catch(Exception e) { //this generic but you can control another types of exception
                            // look the origin of excption
                        }
                    }

                    if (answerSnapshot.getKey().equals("LocationLat")) {

                        MainScreen.Joycelat = answerSnapshot.getValue(Double.class);

                    }

                    if (answerSnapshot.getKey().equals("LocationLong")) {
                        MainScreen.Joycelng = answerSnapshot.getValue(Double.class);


                    }




                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Distance
                Location selected_location = new Location("locationA");
                selected_location.setLatitude(MainScreen.Julianlat);
                selected_location.setLongitude(MainScreen.Julianlng);
                Location near_locations = new Location("locationB");
                near_locations.setLatitude(MainScreen.Joycelat);
                near_locations.setLongitude(MainScreen.Joycelng);
                double distance = selected_location.distanceTo(near_locations);
                distance = distance / 1000 ;
// Write it to Databnase

                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference mdatabaseReference = database.getReference("Pulse").child("Julian");
                mdatabaseReference.child("LatestPulse").setValue(distance);

                Toast.makeText(JoyceTool.this, "Pulse Sent To Julian!", Toast.LENGTH_LONG).show();
            }
        }, 5000); //Timer is in ms here.

    }
}
