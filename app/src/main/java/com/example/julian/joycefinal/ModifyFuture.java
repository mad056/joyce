package com.example.julian.joycefinal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ModifyFuture extends AppCompatActivity {
    public static ArrayList<Status> PastPlaces = new java.util.ArrayList<>();
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    private ListView list;
int pos;
    private ProgressDialog mProgressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_future);
        list = (ListView) findViewById(R.id.FutureList);
        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, R.layout.mylist, arrayList);
        list.setAdapter(adapter);
        mProgressDialog = new ProgressDialog(this);
        LoadData();
        registerForContextMenu(list);

    }

    private void LoadData() {
        mProgressDialog.setMessage("Loading Data...");
        mProgressDialog.show();
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();


        MaxDatabase.child("FuturePlaces").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                        if (InnerSnapshop.getKey().equals("Name")) {
                            arrayList.add(InnerSnapshop.getValue(String.class));
                            adapter.notifyDataSetChanged();
                        }

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mProgressDialog.dismiss();

    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.FutureList) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle(arrayList.get(info.position));
            String[] menuItems = getResources().getStringArray(R.array.menu4);
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu4);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = arrayList.get(info.position);
        pos = info.position;
// Get the Key of the inspecting element in the DB
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("FuturePlaces").orderByChild("Name").equalTo(listItemName).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    PlaceView.CODEREFERENCEVIEW = answerSnapshot.getKey();
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        if (menuItemName.equals("Modify")) {
            AddFuture.LoadMethod = "Edit";
            startActivity(new Intent(ModifyFuture.this, AddFuture.class));
        }


        if (menuItemName.equals("Been To")) {
            BeenTo();
        }


        if (menuItemName.equals("Delete")) {
            String[] items={"Yes", "No" };
            AlertDialog.Builder itemDilog2 = new AlertDialog.Builder(ModifyFuture.this);
            itemDilog2.setTitle("Are You Sure?");
            itemDilog2.setCancelable(true);

            itemDilog2.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch(which) {
                        case 0: {
                            Toast.makeText(ModifyFuture.this, "Deleted", Toast.LENGTH_LONG).show();
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference mdatabaseReference = database.getReference();
                            mdatabaseReference.child("FuturePlaces").child(PlaceView.CODEREFERENCEVIEW).removeValue();
                            arrayList.remove(pos);
                            adapter.notifyDataSetChanged();
                        }
                        break;
                        case 1: {

                        }
                        break;
                    }
                }
            });
            itemDilog2.show();
        }
        return true;
    }

    public void BeenTo() {
        mProgressDialog.setMessage("Loading Data...");
        mProgressDialog.show();
        AddPast.LoadMethod = "Future Convert";
        startActivity(new Intent(ModifyFuture.this, AddPast.class));


        mProgressDialog.dismiss();


    }


}
