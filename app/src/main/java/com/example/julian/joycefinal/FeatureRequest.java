package com.example.julian.joycefinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class FeatureRequest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feature_request);
    }

    public void SubmitButton(View view) {
        final EditText des = (EditText) findViewById(R.id.Edit2);
        final EditText name = (EditText) findViewById(R.id.Edit1);


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("FeatureRequest");
        mdatabaseReference.child(name.getText().toString()).setValue(des.getText().toString());
        Toast.makeText(getApplicationContext(), "Thanks for your request la!", Toast.LENGTH_LONG).show();



        startActivity(new Intent(FeatureRequest.this, MainScreen.class));

    }
}
