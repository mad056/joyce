package com.example.julian.joycefinal;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static android.support.v4.content.ContextCompat.startActivity;

public class SliderAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    Button btn;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    public int[] slider_images = {

            R.drawable.ic_access_time_black_24dp,
            R.mipmap.heart,
            R.mipmap.heart,
            R.drawable.ic_flash_on_black_50dp,
            R.drawable.ic_chat_bubble_black_24dp,
            R.drawable.ic_cake_black_24dp
    };

    public String[] slider_headings = {

            "Every Moment Counts",
            "An Ode To Us",
            "An Ode To Us",
            "Introducing Pulse",
            "Introducing 100 Reasons",
            "The Future"

    };

    public String[] slider_dec = {

            "182 Days, 4380 Hours, 15767998 Seconds.  Over 60 Locations. Not a second ever spent regretting it ",
            "From the shadows of the valley to the calmness of the Sydney sea. No matter the challenge, i only see you and me.",
            "It's only been six months, but can't people see, it is painfully obvious we were meant to be.",
            "Upon clicking pulse. The distance between us is calculated and a notification is received on my phone!",
            "Clique with a hint of 21st century. Generate reasons why i love you and thoughts i have.",
            "This app, just like us, has no limits. Keep a look out for future updates la! Happy 6 Months."
    };

    @Override
    public int getCount() {
        return slider_headings.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == (RelativeLayout) o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate((R.layout.slide_layout), container, false);


        ImageView SliderimageView = (ImageView) view.findViewById(R.id.SliderimageView);
        TextView SlideHeading = (TextView) view.findViewById(R.id.textView16);
        TextView SlideText = (TextView) view.findViewById(R.id.textView15);
        SliderimageView.setImageResource(slider_images[position]);
        SlideHeading.setText(slider_headings[position]);
        SlideText.setText(slider_dec[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}



