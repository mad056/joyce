package com.example.julian.joycefinal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Reasons extends AppCompatActivity {


    public static ArrayList<Status> Reasons = new java.util.ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_reasons);
        final SharedPreferences settings = this.getSharedPreferences("TickCheck", 0);
        long firstTime = settings.getLong("CurrentTick", 0);
        final TextView waittext = (TextView) findViewById(R.id.textView25);
        final TextView headingtext = (TextView) findViewById(R.id.HEadingAns);
        final TextView destext = (TextView) findViewById(R.id.answer);
        // Generate Reasons
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();


        MaxDatabase.child("Reasons").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    Status status = new Status();

                    status.SetStatus(answerSnapshot.getKey());
                    status.SetStatusDes(answerSnapshot.getValue(String.class));
                    Reasons.add(status);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        if (firstTime == 0) {
            // lmao nothing

        } else {
            headingtext.setText(settings.getString("CurrentHeading", ""));
            destext.setText(settings.getString("CurrentStatus", ""));

            new CountDownTimer(firstTime, 1000) {
                public void onTick(long millisUntilFinished) {
                    waittext.setText("Seconds Remaining: " + millisUntilFinished / 1000);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putLong("CurrentTick", millisUntilFinished);
                    editor.commit();
                }

                public void onFinish() {
                    waittext.setText("Ready!");
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putLong("CurrentTick", 0);
                    editor.commit();
                }
            }.start();
        }

        destext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            if (destext.getText().toString().contains("http")) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(destext.getText().toString()));
                startActivity(browserIntent);
            }
            }
        });
    }

    public void GenerateBtnClicked(View view) {
        final SharedPreferences settings = this.getSharedPreferences("TickCheck", 0);
        long firstTime = settings.getLong("CurrentTick", 0);
        final TextView waittext = (TextView) findViewById(R.id.textView25);
        final TextView headingtext = (TextView) findViewById(R.id.HEadingAns);
        final TextView destext = (TextView) findViewById(R.id.answer);

        if (waittext.getText().equals("Ready!")) {
            int tempint = LoadRandom();
            headingtext.setText(Reasons.get(tempint).GetStatus());
            destext.setText(Reasons.get(tempint).GetStatusDes());
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("CurrentHeading", Reasons.get(tempint).GetStatus());
            editor.putString("CurrentStatus", Reasons.get(tempint).GetStatusDes());
            editor.commit();

            new CountDownTimer(43200000, 1000) {

                public void onTick(long millisUntilFinished) {
                    waittext.setText("Seconds Remaining: " + millisUntilFinished / 1000);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putLong("CurrentTick", millisUntilFinished);
                    editor.commit();
                }

                public void onFinish() {
                    waittext.setText("Ready!");
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putLong("CurrentTick", 0);
                    editor.commit();
                }
            }.start();
        } else {
            Toast.makeText(Reasons.this, "Please Wait!.", Toast.LENGTH_LONG).show();

        }


    }
    protected int LoadRandom() {
        Random rand = new Random();

        int  n = rand.nextInt(Reasons.size()) ;
        return n;

    }



    protected String PlusTime() throws ParseException {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR,12);
        System.out.println(now.getTime());

        return String.valueOf(now.getTime());
    }


}
