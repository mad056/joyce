package com.example.julian.joycefinal;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;

import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;

public class AddPast extends AppCompatActivity {
int Position;
    String listItemName;
    private ListView list;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    static final int PICK_MAP_POINT_REQUEST = 999;  // The request code
    static final int FILE_REQUEST = 111;  // The request code

    private static final String TAG = "AddPast";
    private ProgressDialog mProgressDialog ;
    String tempname;
    Integer TempCount ;
    String tempurl;

    public static String LoadMethod;
    private Integer Count;

    private int index;
    private double lat;
    private double loong;
    private String comment;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout2);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        index = 1;
        SelectView(index);


        mProgressDialog = new ProgressDialog(this);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        list = (ListView) findViewById(R.id.ImageAddedList);
        arrayList = new ArrayList<String>();

        adapter = new ArrayAdapter<String>(this, R.layout.mylist, arrayList);
        list.setAdapter(adapter);
        registerForContextMenu(list);

        if (LoadMethod.equals("Edit")) {
            LoadFromEdit();
            LoadImagesIntoList();
        }

        if (LoadMethod.equals("Create")) {
            LoadCount();

        }
        if (LoadMethod.equals("Future Convert")) {
            LoadCount();
            FutureConvertLoad();

        }
    }


    public void SubmitEntryBtn(View view) {
        if (index == 2) {
            final EditText Commedit = (EditText) findViewById(R.id.commentEdit);
            final EditText nameedit = (EditText) findViewById(R.id.nameEdit);

            name = nameedit.getText().toString();
            comment = Commedit.getText().toString();

        }
  ;
        if (index == 4) {

            if (LoadMethod.equals("Edit")) {

                submitEdit();
            }

            if (LoadMethod.equals("Create")) {
                submitCreate();

            }
            if (LoadMethod.equals("Future Convert")) {
                submitCreate();
                FutureConvertSubmit();

            }


        } else {
            index += 1;
            SelectView(index);
        }

       /*

*/
    }

   private void FutureConvertLoad() {
       final EditText Name = (EditText) findViewById(R.id.nameEdit);
       final EditText Comment = (EditText) findViewById(R.id.commentEdit);
       final EditText Lat = (EditText) findViewById(R.id.latEdit);
       final EditText Long = (EditText) findViewById(R.id.longEdit);

       FirebaseDatabase database = FirebaseDatabase.getInstance();
       DatabaseReference mdatabaseReference = database.getReference();
       mdatabaseReference.child("FuturePlaces").child(PlaceView.CODEREFERENCEVIEW).addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(DataSnapshot dataSnapshot) {
               for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                   if (answerSnapshot.getKey().equals("Name")) {
                       Name.setText(answerSnapshot.getValue(String.class));
                       name = answerSnapshot.getValue(String.class);
                       //System.out.println(Name);
                   }


                   if (answerSnapshot.getKey().equals("Lat")) {
                       Lat.setText(String.valueOf(answerSnapshot.getValue(Double.class)));
                       lat = answerSnapshot.getValue(Double.class);


                   }

                   if (answerSnapshot.getKey().equals("Long")) {
                       Long.setText(String.valueOf(answerSnapshot.getValue(Double.class)));
                       loong = answerSnapshot.getValue(Double.class);

                   }

                   if (answerSnapshot.getKey().equals("JoyceComment")) {
                       Comment.setText(answerSnapshot.getValue(String.class));
                      comment = answerSnapshot.getValue(String.class);
                   }
               }
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });
   }

    private void LowerFutureCount() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("FuturePlaces").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals("Count")) {
                        TempCount = answerSnapshot.getValue(Integer.class);
                        TempCount -= 1;

                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    private void FutureConvertSubmit() {
        LowerFutureCount();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference();
        mdatabaseReference.child("FuturePlaces").child(PlaceView.CODEREFERENCEVIEW).removeValue();


    }

    private void submitEdit() {
        final EditText Name = (EditText) findViewById(R.id.nameEdit);
        final EditText Comment = (EditText) findViewById(R.id.commentEdit);
        final EditText Lat = (EditText) findViewById(R.id.latEdit);
        final EditText Long = (EditText) findViewById(R.id.longEdit);
        String Templat = Lat.getText().toString();
        String Templong = Long.getText().toString();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Places/" + PlaceView.CODEREFERENCEVIEW);
        mdatabaseReference.child("JoyceComment").setValue(Comment.getText().toString());
        mdatabaseReference.child("Comment").setValue(" ");

        mdatabaseReference.child("Name").setValue(Name.getText().toString());
        mdatabaseReference.child("Lat").setValue(Double.valueOf(Templat));
        mdatabaseReference.child("Long").setValue(Double.valueOf(Templong));
        Toast.makeText(AddPast.this, "Edited to Database. Success la!", Toast.LENGTH_LONG).show();
        startActivity(new Intent(AddPast.this, PastNavigate.class));
    }


        private void submitCreate() {
        final EditText Name = (EditText) findViewById(R.id.nameEdit);
        final EditText Comment = (EditText) findViewById(R.id.commentEdit);
        final EditText Lat = (EditText) findViewById(R.id.latEdit);
        final EditText Long = (EditText) findViewById(R.id.longEdit);
        Upcount(String.valueOf(Count));
        String Templat = Lat.getText().toString();
        String Templong = Long.getText().toString();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Places/" + Count);
        mdatabaseReference.child("JoyceComment").setValue(Comment.getText().toString());
        mdatabaseReference.child("Comment").setValue(" ");
        mdatabaseReference.child("Name").setValue(Name.getText().toString());
        mdatabaseReference.child("Lat").setValue(Double.valueOf(Templat));
        mdatabaseReference.child("Long").setValue(Double.valueOf(Templong));

            Toast.makeText(AddPast.this, "Written to Database. Success la!", Toast.LENGTH_LONG).show();
            startActivity(new Intent(AddPast.this, PastNavigate.class));
    }

    public void AddLoc(View view) {
        pickPointOnMap();

    }

    private void pickPointOnMap() {
        Intent pickPointIntent = new Intent(this, PickMap.class);


        startActivityForResult(pickPointIntent, PICK_MAP_POINT_REQUEST);
    }


    public void LoadFromEdit() {
        final EditText Name = (EditText) findViewById(R.id.nameEdit);
        final EditText Comment = (EditText) findViewById(R.id.commentEdit);
        final EditText Lat = (EditText) findViewById(R.id.latEdit);
        final EditText Long = (EditText) findViewById(R.id.longEdit);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference();
        mdatabaseReference.child("Places").child(PlaceView.CODEREFERENCEVIEW).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               Count = Integer.valueOf(dataSnapshot.getKey());
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals("Name")) {
                        name = answerSnapshot.getValue(String.class);
                        Name.setText(answerSnapshot.getValue(String.class));
                        //System.out.println(Name);
                    }


                    if (answerSnapshot.getKey().equals("Lat")) {
                        lat = answerSnapshot.getValue(Double.class);
                        Lat.setText(String.valueOf(answerSnapshot.getValue(Double.class)));

                    }

                    if (answerSnapshot.getKey().equals("Long")) {
                        loong = answerSnapshot.getValue(Double.class);
                        Long.setText(String.valueOf(answerSnapshot.getValue(Double.class)));

                    }

                    if (answerSnapshot.getKey().equals("JoyceComment")) {
                        comment = answerSnapshot.getValue(String.class);
                        Comment.setText(answerSnapshot.getValue(String.class));
                    }


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


            protected void LoadCount() {

                FirebaseDatabase data = FirebaseDatabase.getInstance();
                DatabaseReference MaxDatabase = data.getReference();
                MaxDatabase.child("Places").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                            if (answerSnapshot.getKey().equals("Count")) {
                                Count = answerSnapshot.getValue(Integer.class);
                                Count += 1;
                                System.out.println("FUCKING COUNT" + String.valueOf(Count));
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            protected void Upcount(String A) {

                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference mdatabaseReference = database.getReference("Places");
                mdatabaseReference.child("Count").setValue(Integer.valueOf(A));


            }

       protected void UpFuturecount(String A) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("FuturePlaces");
        mdatabaseReference.child("Count").setValue(Integer.valueOf(A));


      }

      private void LoadImagesIntoList() {
          FirebaseDatabase data = FirebaseDatabase.getInstance();
          DatabaseReference MaxDatabase = data.getReference();
          MaxDatabase.child("PlaceImages").child(PlaceView.CODEREFERENCEVIEW).addValueEventListener(new ValueEventListener() {
              @Override
              public void onDataChange(DataSnapshot dataSnapshot) {
                  for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                      arrayList.add(answerSnapshot.getKey());
                      adapter.notifyDataSetChanged();

                  }
              }

              @Override
              public void onCancelled(@NonNull DatabaseError databaseError) {

              }
          });
      }



            public void AddPastImage(View view) {
                Intent intent = new Intent();
                //sets the select file to all types of files
                intent.setType("image/*");
                //allows to select data and return it
                intent.setAction(Intent.ACTION_PICK);
                //starts new activity to select file and return data
                startActivityForResult(Intent.createChooser(intent, "Choose Image to Upload.."), FILE_REQUEST);

            }

            @Override
            protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
                try {
                    super.onActivityResult(requestCode, resultCode, data);
                    if (resultCode == Activity.RESULT_OK) {

                        switch (requestCode) {
                            case 111:
                                mProgressDialog.setMessage("Uploading...");
                                mProgressDialog.show();
                                if (data == null) {
                                    Toast.makeText(AddPast.this, "No Data Found. Try Again..", Toast.LENGTH_LONG).show();

                                    return;
                                }

                                final Uri selectedFileUri = data.getData();
                                File objFile = new File(selectedFileUri.getPath());
                                String tempfilename = objFile.getName();
                                final String fileNameWithOutExt = FilenameUtils.removeExtension(tempfilename);

                                System.out.println("NIGGER " + tempfilename);

                                FirebaseStorage storage = FirebaseStorage.getInstance();
                                final StorageReference storageRef = storage.getReference();
                                final StorageReference filepath = storageRef.child("PastImages" + "/ " + Count).child(fileNameWithOutExt);
                                //tempname = selectedFileUri.getLastPathSegment();
                                 arrayList.add(fileNameWithOutExt);
                                //adapter.notifyDataSetChanged();


                                filepath.putFile(selectedFileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                        filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                                                           @Override
                                                                                           public void onSuccess(Uri uri) {

                                                                                               Log.wtf(TAG, "download image path : " + uri.toString());
                                                                                               //now you have path to the uploaded file save this path to your database
                                                                                               FirebaseDatabase database2 = FirebaseDatabase.getInstance();
                                                                                               DatabaseReference mdatabaseReference2 = database2.getReference("PlaceImages/" + Count);
                                                                                               Toast.makeText(AddPast.this, uri.toString(), Toast.LENGTH_LONG).show();
                                                                                               mdatabaseReference2.child(fileNameWithOutExt).setValue(uri.toString());

                                                                                           }
                                                                                       });
                                        // Filestore path


                                        Toast.makeText(AddPast.this, "UPLOAD DONE", Toast.LENGTH_LONG).show();

                                        mProgressDialog.dismiss();


                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(AddPast.this, "UPLOAD FAILED", Toast.LENGTH_LONG).show();

                                    }
                                });
                                break;

                            case 999:
                                if (resultCode == RESULT_OK) {
                                    LatLng latLng = (LatLng) data.getParcelableExtra("picked_point");
                                    String templocname = (String) data.getStringExtra("title");
                                    final EditText lat2 = (EditText) findViewById(R.id.longEdit);
                                    final EditText long2 = (EditText) findViewById(R.id.latEdit);

                                    lat2.setText(String.valueOf(latLng.latitude));
                                    long2.setText(String.valueOf(latLng.longitude));

                                    lat = latLng.latitude;
                                    loong = latLng.longitude;
                                    name = templocname;

                                    // Toast.makeText(this, "Point Chosen: " + latLng.latitude + " " + latLng.longitude, Toast.LENGTH_LONG).show();
                                }
                        }


                    }

                } catch (SecurityException e) {
                    new AlertDialog.Builder(this)
                            .setTitle("Permission Error")
                            .setMessage("Please Enable Read Storage Permissions For The Application To Work")
                            .setPositiveButton(android.R.string.ok, null)
                            .show();

                }
            }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.ImageAddedList) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle(arrayList.get(info.position));
            listItemName  = arrayList.get(info.position);
            // Get the DL URL of the inspecting element in the DB
            FirebaseDatabase data = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = data.getReference();
            MaxDatabase.child("PlaceImages").child(String.valueOf(Count)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        if (answerSnapshot.getKey().equals(listItemName)) {
                            PlaceView.IMAGEREF = answerSnapshot.getValue(String.class);
                        }
                    }

                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            String[] menuItems = getResources().getStringArray(R.array.menu5);
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu5);
         String menuItemName = menuItems[menuItemIndex];
        listItemName  = arrayList.get(info.position);
        Position = info.position;



        if (menuItemName.equals("View")) {
            Toast.makeText(AddPast.this, "Not Implemented Yet!!", Toast.LENGTH_LONG).show();
        }


        if (menuItemName.equals("Delete")) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference photoRef = storage.getReferenceFromUrl(PlaceView.IMAGEREF);
            photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // File deleted successfully
                    Toast.makeText(AddPast.this, "Deleted!", Toast.LENGTH_LONG).show();
                    arrayList.remove(Position);
                    adapter.notifyDataSetChanged();
                    Log.d(TAG, "onSuccess: deleted file");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Uh-oh, an error occurred!
                    Toast.makeText(AddPast.this, "Error! Tell Julian. He Fucked up!", Toast.LENGTH_LONG).show();

                    Log.d(TAG, "onFailure: did not delete file");
                }
            });

            // Delete Referenace From Database

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference mdatabaseReference = database.getReference();
            mdatabaseReference.child("PlaceImages").child(String.valueOf(Count)).child(listItemName).removeValue();

        }





        return true;
    }

    @Override
    public void onBackPressed() {
    }

    public void SelectView(int i) {
        final TextView Name = (TextView) findViewById(R.id.textView18);
        final TextView ClicktoBegin = (TextView) findViewById(R.id.textView19);
        final ImageButton AddImage = (ImageButton) findViewById(R.id.addPastImage);
        final ImageButton SetLoc = (ImageButton) findViewById(R.id.SetLoc);
        final ListView ImagesAddedList = (ListView) findViewById(R.id.ImageAddedList);
        final EditText Latedit = (EditText) findViewById(R.id.latEdit);
        final EditText LongEdit = (EditText) findViewById(R.id.longEdit);
        final ImageButton Submit = (ImageButton) findViewById(R.id.submitEntryBtn3);
        final EditText Commedit = (EditText) findViewById(R.id.commentEdit);
        final EditText nameedit = (EditText) findViewById(R.id.nameEdit);

        final TextView Heading = (TextView) findViewById(R.id.addImagesText);
        final TextView commentname = (TextView) findViewById(R.id.textView17);
        RelativeLayout rl = (RelativeLayout)findViewById(R.id.relativeLayout);

        switch (i) {

            case 1 :
                Name.setVisibility(View.INVISIBLE);
                AddImage.setVisibility(View.INVISIBLE);
                AddImage.setEnabled(false);
                ImagesAddedList.setVisibility(View.INVISIBLE);
                ImagesAddedList.setEnabled(false);
                Commedit.setVisibility(View.INVISIBLE);
                ImagesAddedList.setEnabled(false);
                nameedit.setEnabled(false);
                nameedit.setVisibility(View.INVISIBLE);
                commentname.setVisibility(View.INVISIBLE);

                Heading.setText("Select Location");
                Submit.setImageResource(R.drawable.continuepur);
                ClicktoBegin.setVisibility(View.VISIBLE);
                SetLoc.setVisibility(View.VISIBLE);
                SetLoc.setEnabled(true);
                Latedit.setVisibility(View.VISIBLE);
                LongEdit.setVisibility(View.VISIBLE);



                break;
            case 2 :
try {
    lat = Double.parseDouble(Latedit.getText().toString());
    loong = Double.parseDouble(LongEdit.getText().toString());
}catch(Exception lmao) {
    Toast.makeText(AddPast.this, "Select A Location...", Toast.LENGTH_LONG).show();
    startActivity(new Intent(AddPast.this, PastNavigate.class));

}
                AddImage.setVisibility(View.INVISIBLE);
                AddImage.setEnabled(false);
                ImagesAddedList.setVisibility(View.INVISIBLE);
                ImagesAddedList.setEnabled(false);
                ImagesAddedList.setEnabled(false);
                SetLoc.setVisibility(View.INVISIBLE);
                SetLoc.setEnabled(false);
                Submit.setBackgroundColor(0x41A9CC);
                ClicktoBegin.setVisibility(View.INVISIBLE);



                rl.setBackgroundColor(getResources().getColor(R.color.backgroundvlue));

                commentname.setVisibility(View.VISIBLE);
                Commedit.setVisibility(View.VISIBLE);
                Name.setVisibility(View.VISIBLE);
                Latedit.setVisibility(View.VISIBLE);
                LongEdit.setVisibility(View.VISIBLE);
                Heading.setText("Add Comment");
                Submit.setImageResource(R.drawable.continueblue);
                nameedit.setEnabled(true);
                nameedit.setVisibility(View.VISIBLE);
                nameedit.setText(name);

                break;
            case 3 :
                Heading.setText("Add Images");
                AddImage.setVisibility(View.VISIBLE);
                AddImage.setEnabled(true);
                ImagesAddedList.setVisibility(View.VISIBLE);
                ImagesAddedList.setEnabled(true);
                SetLoc.setVisibility(View.INVISIBLE);
                SetLoc.setEnabled(false);
                Submit.setBackgroundColor(0xE1625C);
                ClicktoBegin.setVisibility(View.VISIBLE);


                rl.setBackgroundColor(getResources().getColor(R.color.backgroundborange));

                commentname.setVisibility(View.INVISIBLE);
                Commedit.setVisibility(View.INVISIBLE);
                Name.setVisibility(View.INVISIBLE);
                Latedit.setVisibility(View.INVISIBLE);
                LongEdit.setVisibility(View.INVISIBLE);
                Submit.setImageResource(R.drawable.vontinueor);
                nameedit.setEnabled(false);
                nameedit.setVisibility(View.INVISIBLE);
                nameedit.setText(name);



                break;

            case 4 :
                Heading.setText("Summary");
                AddImage.setVisibility(View.INVISIBLE);
                AddImage.setEnabled(false);
                ImagesAddedList.setVisibility(View.VISIBLE);
                ImagesAddedList.setEnabled(true);
                SetLoc.setVisibility(View.INVISIBLE);
                SetLoc.setEnabled(false);
                Submit.setBackgroundColor(0xFDBE57);
                ClicktoBegin.setVisibility(View.INVISIBLE);
                nameedit.setText(name);

                Commedit.setText(comment);
                for (int j = 0; i < arrayList.size(); ++i) {
                    adapter.notifyDataSetChanged();

                }

                rl.setBackgroundColor(getResources().getColor(R.color.backorange));

                commentname.setVisibility(View.VISIBLE);
                Commedit.setVisibility(View.VISIBLE);
                Name.setVisibility(View.VISIBLE);
                Latedit.setVisibility(View.INVISIBLE);
                LongEdit.setVisibility(View.INVISIBLE);
                Submit.setImageResource(R.drawable.gotitornage);
                nameedit.setEnabled(false);
                Commedit.setEnabled(false);
                nameedit.setVisibility(View.VISIBLE);
                break;

        }

    }

    public void CancelClick(View view) {
        mProgressDialog.setMessage("Canceling Please Wait");
mProgressDialog.show();

        if (LoadMethod.equals("Edit")) {
            mProgressDialog.dismiss();

            Toast.makeText(AddPast.this, "Canceled la!", Toast.LENGTH_LONG).show();
            startActivity(new Intent(AddPast.this, PastNavigate.class));


        } else {

            for (int i = 0; i < arrayList.size(); ++i) {
                final int finalI1 = i;

                FirebaseDatabase data = FirebaseDatabase.getInstance();
                DatabaseReference MaxDatabase = data.getReference();
                MaxDatabase.child("PlaceImages").child(String.valueOf(Count)).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                            if (answerSnapshot.getKey().equals(arrayList.get(finalI1).toString())) {
                                PlaceView.IMAGEREF = answerSnapshot.getValue(String.class);
                            }
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FirebaseStorage storage = FirebaseStorage.getInstance();
                        StorageReference photoRef = storage.getReferenceFromUrl(PlaceView.IMAGEREF);

                        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                // File deleted successfully
                                Toast.makeText(AddPast.this, "Deleted!", Toast.LENGTH_LONG).show();
                                arrayList.remove(finalI1);
                                adapter.notifyDataSetChanged();
                                Log.d(TAG, "onSuccess: deleted file");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Uh-oh, an error occurred!
                                Toast.makeText(AddPast.this, "Error! Tell Julian. He Fucked up!", Toast.LENGTH_LONG).show();

                                Log.d(TAG, "onFailure: did not delete file");
                            }
                        });
                        // Delete Referenace From Database

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference mdatabaseReference = database.getReference();
                        mdatabaseReference.child("PlaceImages").child(String.valueOf(Count)).child(arrayList.get(finalI1).toString()).removeValue();
                    }
                }, 2000); //Timer is in ms here.

            }
            mProgressDialog.dismiss();

            Toast.makeText(AddPast.this, "Canceled la!", Toast.LENGTH_LONG).show();
            startActivity(new Intent(AddPast.this, PastNavigate.class));

        }
    }





}
